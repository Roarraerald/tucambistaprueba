
import React, {Component, useEffect, useState} from 'react';
//import type {Node} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  TouchableOpacity,
  useColorScheme,
  View,
  Dimensions,
  TextInput,
} from 'react-native';

const ComponentType = () => {
  const [data, setData] = useState({});

  const [compra, setCompra] = useState('');

  const [venta, setVenta] = useState('');

  const [text, onChangeText] = React.useState('0');

  useEffect(async () => {
    try {
      let response = await fetch('http://app.tucambista.pe/api/tipodecambio/');
      let json = await response.json();
      //setData(json.data);
      setCompra(json.compra)
      setVenta(json.venta)
      console.warn('json', json);
      return json;
    } catch (error) {
      console.error(error);
    }
  });

  return (
    <View style={{marginLeft: 4, marginRight: 4}}>
      <Text style={{textAlign: 'center'}}>Tipo de cambio</Text>
      <View style={{flexDirection: 'row', marginBottom: 8}}>
        <View style={{width: Dimensions.get('window').width / 2}}>
          <Text>Compras: {compra}</Text>
          <TextInput></TextInput>
        </View>
        <View style={{width: Dimensions.get('window').width / 2}}>
          <Text> Venta: {venta}</Text>
        </View>
      </View>

      <View
        style={{
          flexDirection: 'row',
          marginRight: 8,
          marginLeft: 8,
          borderColor: 'gray',
          borderWidth: 1,
          borderRadius: 8,
          paddingLeft: 5,
          paddingRight: 5,
        }}>
        <View style={{width: Dimensions.get('window').width / 2}}>
          <Text>Tu recibes</Text>
          <View style={{height: 40}}>
          <TextInput
            onChangeText={onChangeText}
            value={text}
          />
          </View>
        </View>
        <View style={{width: Dimensions.get('window').width / 2}}>
          <Text> DOLARES</Text>
        </View>
      </View>
      <View
        style={{
          flexDirection: 'row',
          marginRight: 8,
          marginLeft: 8,
          borderColor: 'gray',
          borderWidth: 1,
          borderRadius: 8,
          paddingLeft: 5,
          paddingRight: 5,
          marginTop: 10,
        }}>
        <View style={{width: Dimensions.get('window').width / 2, height: 40 }}>
          <Text>Tu envias</Text>
          <Text>{Number(text)*Number(compra)}</Text>
        </View>
        <View style={{width: Dimensions.get('window').width / 2}}>
          <Text> SOLES</Text>
        </View>
      </View>
    </View>
  );
};

export default ComponentType