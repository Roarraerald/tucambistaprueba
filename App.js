/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {Component, useEffect, useState} from 'react';
//import type {Node} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  TouchableOpacity,
  useColorScheme,
  View,
  Dimensions,
  TextInput,
} from 'react-native';

import {
  Colors,
  DebugInstructions,
  Header,
  LearnMoreLinks,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';

import HomeScreen from './Home';
import DetailScreen from './Details';

//import ComponentType from './src/components/ComponentType'

const getArticlesFromApi = async () => {
  try {
    let response = await fetch('https://reqres.in/api/users?page=2');
    let json = await response.json();
    console.warn('json', json);
    return json.movies;
  } catch (error) {
    console.error(error);
  }
};

/*export class App extends Component {
  constructor(props) {
    super(props);
    //this

    this.state = {
      count: 0,
      data:[],
      countries: [
        {id: 0, country: 'Peru'},
        {id: 1, country: 'Arequipa'},
      ],
    };
  }

  countFunction(count) {
    this.setState({count: count + 1});
  }

  getArticlesFromApi = async () => {
    try {
      let response = await fetch(
        'https://reqres.in/api/users?page=2'
      );
      let json = await response.json();
      console.warn('json', json)
      this.setState({data: json.data})
      return json.movies;
    } catch (error) {
       console.error(error);
    }
  };
  
  

  render() {
    return (
      <View style={{flex: 1, backgroundColor: 'orange'}}>
        <View style={{flexDirection: 'row'}}>
          <View
            style={{
              backgroundColor: 'red',
              width: Dimensions.get('window').width / 4,
            }}>
            <Text style={{color: 'white'}}>Test</Text>
          </View>
          <View
            style={{
              backgroundColor: 'blue',
              width: Dimensions.get('window').width / 4,
            }}>
            <Text style={{color: 'white'}}>Test</Text>
          </View>
        </View>

        <View style={{height: 40, backgroundColor: 'blue'}}>
          {this.state.data.map(item => {
            return <Text>{item.first_name} {item.last_name}</Text>;
          })}
        </View>

      <View
          style={{
            backgroundColor: 'blue',
            width: Dimensions.get('window').width / 2,
          }}>
          <Text style={{color: 'white'}}>Test</Text>
        </View>

        <View style={{alignItems: 'center'}}>
          <Text>Un amigo</Text>
        </View>

        <View style={{alignItems: 'center'}}>
          <Text>Un amigo {this.state.count}</Text>
        </View>

        <TouchableOpacity onPress={() => this.countFunction(this.state.count)}>
          <Text>Suma</Text>
        </TouchableOpacity>
      </View>
    );
  }
}*/

//Hooks

const App = () => {
  const [data, setData] = useState({});

  const [compra, setCompra] = useState('');

  const [venta, setVenta] = useState('');

  const [text, onChangeText] = React.useState('0');

  useEffect(async () => {
    try {
      let response = await fetch('http://app.tucambista.pe/api/tipodecambio/');
      let json = await response.json();
      //setData(json.data);
      setCompra(json.compra)
      setVenta(json.venta)
      console.warn('json', json);
      return json;
    } catch (error) {
      console.error(error);
    }
  });

  return (
    <View style={{marginLeft: 4, marginRight: 4}}>
      <Text style={{textAlign: 'center'}}>Tipo de cambio</Text>
      <View style={{flexDirection: 'row', marginBottom: 8}}>
        <View style={{width: Dimensions.get('window').width / 2}}>
          <Text>Compra: {compra}</Text>
          <TextInput></TextInput>
        </View>
        <View style={{width: Dimensions.get('window').width / 2}}>
          <Text> Venta: {venta}</Text>
        </View>
      </View>

      <View
        style={{
          flexDirection: 'row',
          marginRight: 8,
          marginLeft: 8,
          borderColor: 'gray',
          borderWidth: 1,
          borderRadius: 8,
          paddingLeft: 5,
          paddingRight: 5,
        }}>
        <View style={{width: Dimensions.get('window').width / 2}}>
          <Text>Tu recibes</Text>
          <View style={{height: 40}}>
          <TextInput
            onChangeText={onChangeText}
            value={text}
          />
          </View>
        </View>
        <View style={{width: Dimensions.get('window').width / 2}}>
          <Text> DOLARES</Text>
        </View>
      </View>
      <View
        style={{
          flexDirection: 'row',
          marginRight: 8,
          marginLeft: 8,
          borderColor: 'gray',
          borderWidth: 1,
          borderRadius: 8,
          paddingLeft: 5,
          paddingRight: 5,
          marginTop: 10,
        }}>
        <View style={{width: Dimensions.get('window').width / 2, height: 40 }}>
          <Text>Tu envias</Text>
          <Text>{Number(text)*Number(compra)}</Text>
        </View>
        <View style={{width: Dimensions.get('window').width / 2}}>
          <Text> SOLES</Text>
        </View>
      </View>
    </View>
  );
};



/*

const Stack = createStackNavigator();

export const App = (navigation) => {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="ComponentType" component={ComponentType} />
        <Stack.Screen name="Detail" component={DetailScreen}/>
      </Stack.Navigator>
    </NavigationContainer>
  );
}
*/

const styles = StyleSheet.create({
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
  },
  highlight: {
    fontWeight: '700',
  },
});

export default App;
